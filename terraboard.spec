%define debug_package %{nil}

Name:           terraboard
Version:        2.4.0
Release:        0%{?dist}
Summary:        A web dashboard to inspect Terraform States
Group:          Applications/System
License:        ASL 2.0
URL:            https://terraboard.io/
Source0:        https://github.com/camptocamp/%{name}/releases/download/v%{version}/%{name}_%{version}_linux_amd64.zip

%description
A web dashboard to inspect Terraform States

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
install -p -m 755 %{name}_v%{version} %{buildroot}/%{_bindir}/%{name}

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.3.0

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM